import { Component, Input, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent {
    getData() {
        console.log(this.baseUrl);
        this.http.get(this.baseUrl + 'api/InventoryMasterAPI/Inventory').subscribe(result => {
            this.Inventory = result.json();
        }, error => console.error(error));
    }
    public Inventory: InventoryMaster[] = [];
    AddTable: Boolean = false;

    sInventoryId: number=0;
    sInventoryName: string="";
    sStockQty: number=0;
    sReorderQty: number = 0;
    sPriorityStatus: boolean = false;

    public imgChk = require("./Images/chk.png");
    public imgUnChk = require("./Images/unchk.png");
    public baseUrl: string = "";

    public sChkName: string = "";
    myName: string;

    constructor(public http: Http, @Inject('BASE_URL') baseurl: string) {
        this.myName = "Alamgir";
        this.AddTable = false;
        this.baseUrl = baseurl;
        this.getData();
    }
    addInventoryDetails(inventoryIDs: number, itemNames: string, stockQtys: number, reorderQtys: number, priorityStatus: boolean) {
        var pStatus: number = 0;
        this.sChkName = priorityStatus.toString();
        if (this.sChkName == 'true') {
            pStatus = 1;
        }
        var headers = new Headers();
        headers.append('Content-type', 'application/json; charset=utf-8');
        if (inventoryIDs == 0) {
            this.http.post(this.baseUrl + 'api/InventoryMasterAPI/', JSON.stringify({
                ItemName: itemNames,StockQty: stockQtys, ReorderQty: reorderQtys, PriorityStatus: pStatus
            }), { headers: headers }).subscribe(
                response => {
                    this.getData();
                }, error => {
                    console.log("save error");
                }
                );
        }
        else {
            this.http.put(this.baseUrl + 'api/InventoryAPI/' + inventoryIDs, JSON.stringify({
                InventoryID: inventoryIDs, ItemName: itemNames, StockQty: stockQtys, ReorderQty: reorderQtys, PriorityStatus: pStatus
            }), { headers: headers }
            ).subscribe(response => {
                this.getData();
            }, error => {
                console.log("update error");
            });
        }
        this.AddTable = false;
    }
    addInventory() {
        this.AddTable = true;
        this.sInventoryId = 0;
        this.sInventoryName = "";
        this.sStockQty = 50;
        this.sReorderQty = 0;
        this.sPriorityStatus = false;
    }
    closeEdits() {
        this.AddTable = false;
        this.sInventoryId = 0;
        this.sInventoryName = "";
        this.sStockQty = 50;
        this.sReorderQty = 0;
        this.sPriorityStatus = false;
    }
}
export interface InventoryMaster {
    inventoryId: number;
    inventoryName: string;
    stockQty: number;
    reorderQty: number;
    priorityStatus: number;
}
